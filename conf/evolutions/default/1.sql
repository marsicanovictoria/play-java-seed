# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table branch_office (
  id                            bigint auto_increment not null,
  rif                           varchar(255),
  name_branch_office            varchar(255),
  address_branch_office         varchar(255),
  constraint pk_branch_office primary key (id)
);

create table company (
  rif                           varchar(255) not null,
  name                          varchar(255),
  address                       varchar(255),
  constraint pk_company primary key (rif)
);

create index ix_branch_office_rif on branch_office (rif);
alter table branch_office add constraint fk_branch_office_rif foreign key (rif) references company (rif) on delete restrict on update restrict;


# --- !Downs

alter table branch_office drop foreign key fk_branch_office_rif;
drop index ix_branch_office_rif on branch_office;

drop table if exists branch_office;

drop table if exists company;

