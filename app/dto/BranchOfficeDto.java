package dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import model.Company;

/**
 * Created by root on 9/3/18.
 */
public class BranchOfficeDto {

    private String nameBranchOffice;
    private String rif;
    private String addressBranchOffice;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("nameBranchOffice")
    public String getNameBranchOffice() {
        return nameBranchOffice;
    }

    public void setNameBranchOffice(String nameBranchOffice) {
        this.nameBranchOffice = nameBranchOffice;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("addressBranchOffice")
    public String getAddressBranchOffice() {
        return addressBranchOffice;
    }

    public void setAddressBranchOffice(String addressBranchOffice) {
        this.addressBranchOffice = addressBranchOffice;
    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("rif")
	public String getRif() {
		return rif;
	}

	public void setRif(String rif) {
		this.rif = rif;
	}
    
    
}
