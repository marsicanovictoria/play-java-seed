package dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by root on 9/3/18.
 */
public class CompanyDto {

    private String rif;
    private String name;
    private String address;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("rif")
    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("email")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
