package services;

import dto.BranchOfficeDto;
import dto.CompanyDto;
import io.ebean.Ebean;
import model.BranchOffice;
import model.Company;
import play.libs.Json;
import play.mvc.Result;

import static play.mvc.Results.*;

/**
 * Created by root on 9/3/18.
 */
public class Services {

    public Result createCompany(CompanyDto companydto) {

        if (companydto.getAddress() == null || companydto.getName() == null
                || companydto.getRif() == null) {
            return notFound();
        }

        Company companyDB = Company.findByRif(companydto.getRif());

        if (companyDB != null) {
            return notFound();
        }

        Company company = new Company();
        company.setRif(companydto.getRif());
        company.setAddress(companydto.getAddress());
        company.setName(companydto.getName());
        company.save();

        return created(Json.toJson(company.getRif()));

    }

    public Result updateCompany(CompanyDto companydto) {

        if (companydto.getAddress() == null && companydto.getName() == null
                && companydto.getRif() == null) {
            return notFound();
        }

        Company company = new Company();
        company.setRif(companydto.getRif());
        company.setAddress(companydto.getAddress());
        company.setName(companydto.getName());
        company.update();

        return created(Json.toJson(companydto));
    }

    public Result getCompany(String rif) {

        if (rif == null) {
            return notFound();
        }
        Company company = Company.findByRif(rif);
        Company companyResponse = new Company();
        companyResponse.setRif(company.getRif());
        companyResponse.setName(company.getName());
        companyResponse.setAddress(company.getAddress());

        return created(Json.toJson(companyResponse));
    }

    public Result createBranchOffice(BranchOfficeDto branchOfficeDto) {

        if (branchOfficeDto.getAddressBranchOffice() == null && branchOfficeDto.getNameBranchOffice() == null
                && branchOfficeDto.getRif() == null) {
            return notFound();
        }

        Company companyDB = Company.findByRif(branchOfficeDto.getRif());

        if (companyDB == null) {
            return notFound();
        }

        BranchOffice branchOffice = new BranchOffice();
        branchOffice.setCompany(companyDB);
        branchOffice.setAddressBranchOffice(branchOfficeDto.getAddressBranchOffice());
        branchOffice.setNameBranchOffice(branchOfficeDto.getNameBranchOffice());
        branchOffice.save();

        return created(Json.toJson(branchOffice.getId()));
    }

    public Result updateBranchOffice(BranchOfficeDto branchOfficeDto) {

        if (branchOfficeDto.getAddressBranchOffice() == null && branchOfficeDto.getNameBranchOffice() == null
                && branchOfficeDto.getRif() == null) {
            return notFound();
        }

        BranchOffice branchOffice = new BranchOffice();
        branchOffice.setAddressBranchOffice(branchOfficeDto.getAddressBranchOffice());
        branchOffice.setNameBranchOffice(branchOfficeDto.getNameBranchOffice());
        branchOffice.update();

        return created(Json.toJson(branchOffice));
    }

    public Result getBranchOffice(Long id) {

        BranchOffice branchOffice = BranchOffice.findById(id);

        if (branchOffice == null) {
            return notFound();
        }

        BranchOffice branchOfficeResponse = new BranchOffice();
        branchOfficeResponse.setNameBranchOffice(branchOffice.getNameBranchOffice());
        branchOfficeResponse.setId(branchOffice.getId());
        branchOfficeResponse.setAddressBranchOffice(branchOffice.getAddressBranchOffice());

        return created(Json.toJson(branchOfficeResponse));
    }

    public Result deleteCompany(String rif) {
        Company company = Company.findByRif(rif);


        Ebean.delete(company);

        return created(Json.toJson(company));

    }

    public Result deleteBranchOffice(Long id) {
        BranchOffice branchOffice = BranchOffice.findById(id);


        Ebean.delete(branchOffice);

        return created(Json.toJson(branchOffice));

    }

}
