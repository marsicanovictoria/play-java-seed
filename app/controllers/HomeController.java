package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import dto.BranchOfficeDto;
import dto.CompanyDto;
import model.BranchOffice;
import play.libs.Json;
import play.mvc.*;
import services.Services;

import javax.inject.Inject;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */

    @Inject
    private Services services;

    public Result createCompany() {
        JsonNode json = request().body().asJson();
        CompanyDto companydto = Json.fromJson(json, CompanyDto.class);
        return services.createCompany(companydto);
    }

    public Result updateCompany() {
        JsonNode json = request().body().asJson();
        CompanyDto companydto = Json.fromJson(json, CompanyDto.class);
        return services.updateCompany(companydto);
    }


    public Result getCompany(String rif) {
        return services.getCompany(rif);
    }

    public Result createBranchOffice() {
        JsonNode json = request().body().asJson();
        BranchOfficeDto branchOfficeDto = Json.fromJson(json, BranchOfficeDto.class);
        return services.createBranchOffice(branchOfficeDto);
    }

    public Result updateBranchOffice() {
        JsonNode json = request().body().asJson();
        BranchOfficeDto branchOfficeDto = Json.fromJson(json, BranchOfficeDto.class);
        return services.updateBranchOffice(branchOfficeDto);
    }

    public Result getBranchOffice(Long id) {
        return services.getBranchOffice(id);
    }
    
    public Result deleteCompany(String rif) {
    	return services.deleteCompany(rif);
    }

    public Result deleteBranchOffice(Long id) {
    	return services.deleteBranchOffice(id);
    }

    //public Result index() {
    //  return ok(views.html.index.render());
    //}

}
