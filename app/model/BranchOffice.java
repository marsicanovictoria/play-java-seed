package model;

import javax.persistence.*;

import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name= "branch_office")
public class BranchOffice extends Model {

	private Long id;
	private Company company;	
	private String nameBranchOffice;
	private String addressBranchOffice;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne
	@JoinColumn(name= "rif", referencedColumnName = "rif")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	@Column(name="name_branch_office")
	public String getNameBranchOffice() {
		return nameBranchOffice;
	}
	public void setNameBranchOffice(String nameBranchOffice) {
		this.nameBranchOffice = nameBranchOffice;
	}
	@Column(name="address_branch_office")
	public String getAddressBranchOffice() {
		return addressBranchOffice;
	}
	public void setAddressBranchOffice(String addressBranchOffice) {
		this.addressBranchOffice = addressBranchOffice;
	}

	public static Finder<Long, BranchOffice> find = new Finder<>(BranchOffice.class);

	public static BranchOffice findByRifBranchOffice(String rif) {
		return BranchOffice.find
				.query()
				.where()
				.eq("rif", rif)
				.findOne();
	}

	public static BranchOffice findById(Long id) {
		return BranchOffice.find
				.query()
				.where()
				.eq("id", id)
				.findOne();
	}
	
}
