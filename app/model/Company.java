package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import io.ebean.Finder;
import io.ebean.Model;

@Entity
@Table(name= "company")
public class Company extends Model {
	
	private String rif;
	private String name;
	private String address;
	private List<BranchOffice> branchOffice; 
	
	@Id
	@Column(name = "rif")
	public String getRif() {
		return rif;
	}
	public void setRif(String rif) {
		this.rif = rif;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Column(name = "address")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@OneToMany(mappedBy="company")
	public List<BranchOffice> getBranchOffice() {
		return branchOffice;
	}
	public void setBranchOffice(List<BranchOffice> branchOffice) {
		this.branchOffice = branchOffice;
	}

	public static Finder<Long, Company> find = new Finder<>(Company.class);

	public static Company findByRif(String rif) {
		return Company.find
				.query()
				.where()
				.eq("rif", rif)
				.findOne();
	}







}
