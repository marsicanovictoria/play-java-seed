name := """play-java-maria"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava,
PlayEbean)

scalaVersion := "2.12.4"

libraryDependencies ++= Seq (
guice,
evolutions,
jdbc,
"org.hibernate" % "hibernate-core" % "5.3.3.Final",
"org.modelmapper" % "modelmapper" % "0.7.7")

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.16"

libraryDependencies += "javax.persistence" % "persistence-api" % "1.0"

