// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Maria Victoria/Documents/play-java-seed/conf/routes
// @DATE:Wed Sep 05 19:18:27 VET 2018


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
