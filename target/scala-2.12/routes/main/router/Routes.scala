// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Maria Victoria/Documents/play-java-seed/conf/routes
// @DATE:Wed Sep 05 19:18:27 VET 2018

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:8
  Assets_1: controllers.Assets,
  // @LINE:19
  HomeController_0: controllers.HomeController,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:8
    Assets_1: controllers.Assets,
    // @LINE:19
    HomeController_0: controllers.HomeController
  ) = this(errorHandler, Assets_1, HomeController_0, "/")

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Assets_1, HomeController_0, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/createCompany""", """controllers.HomeController.createCompany"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/updateCompany""", """controllers.HomeController.updateCompany"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/getCompany/""" + "$" + """rif<[^/]+>""", """controllers.HomeController.getCompany(rif:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/createBranchOffice""", """controllers.HomeController.createBranchOffice"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/updateBranchOffice""", """controllers.HomeController.updateBranchOffice"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/getBranchOffice/""" + "$" + """id<[^/]+>""", """controllers.HomeController.getBranchOffice(id:Long)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/deleteCompany/""" + "$" + """rif<[^/]+>""", """controllers.HomeController.deleteCompany(rif:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """prueba/v1/deleteBranchOffice/""" + "$" + """id<[^/]+>""", """controllers.HomeController.deleteBranchOffice(id:Long)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:8
  private[this] lazy val controllers_Assets_versioned0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned0_invoker = createInvoker(
    Assets_1.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:17
  private[this] lazy val controllers_Assets_versioned1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned1_invoker = createInvoker(
    Assets_1.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )

  // @LINE:19
  private[this] lazy val controllers_HomeController_createCompany2_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/createCompany")))
  )
  private[this] lazy val controllers_HomeController_createCompany2_invoker = createInvoker(
    HomeController_0.createCompany,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "createCompany",
      Nil,
      "POST",
      this.prefix + """prueba/v1/createCompany""",
      """""",
      Seq()
    )
  )

  // @LINE:20
  private[this] lazy val controllers_HomeController_updateCompany3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/updateCompany")))
  )
  private[this] lazy val controllers_HomeController_updateCompany3_invoker = createInvoker(
    HomeController_0.updateCompany,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "updateCompany",
      Nil,
      "POST",
      this.prefix + """prueba/v1/updateCompany""",
      """""",
      Seq()
    )
  )

  // @LINE:21
  private[this] lazy val controllers_HomeController_getCompany4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/getCompany/"), DynamicPart("rif", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_getCompany4_invoker = createInvoker(
    HomeController_0.getCompany(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "getCompany",
      Seq(classOf[String]),
      "GET",
      this.prefix + """prueba/v1/getCompany/""" + "$" + """rif<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:22
  private[this] lazy val controllers_HomeController_createBranchOffice5_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/createBranchOffice")))
  )
  private[this] lazy val controllers_HomeController_createBranchOffice5_invoker = createInvoker(
    HomeController_0.createBranchOffice,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "createBranchOffice",
      Nil,
      "POST",
      this.prefix + """prueba/v1/createBranchOffice""",
      """""",
      Seq()
    )
  )

  // @LINE:23
  private[this] lazy val controllers_HomeController_updateBranchOffice6_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/updateBranchOffice")))
  )
  private[this] lazy val controllers_HomeController_updateBranchOffice6_invoker = createInvoker(
    HomeController_0.updateBranchOffice,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "updateBranchOffice",
      Nil,
      "POST",
      this.prefix + """prueba/v1/updateBranchOffice""",
      """""",
      Seq()
    )
  )

  // @LINE:24
  private[this] lazy val controllers_HomeController_getBranchOffice7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/getBranchOffice/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_getBranchOffice7_invoker = createInvoker(
    HomeController_0.getBranchOffice(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "getBranchOffice",
      Seq(classOf[Long]),
      "GET",
      this.prefix + """prueba/v1/getBranchOffice/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:25
  private[this] lazy val controllers_HomeController_deleteCompany8_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/deleteCompany/"), DynamicPart("rif", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_deleteCompany8_invoker = createInvoker(
    HomeController_0.deleteCompany(fakeValue[String]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "deleteCompany",
      Seq(classOf[String]),
      "POST",
      this.prefix + """prueba/v1/deleteCompany/""" + "$" + """rif<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:26
  private[this] lazy val controllers_HomeController_deleteBranchOffice9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("prueba/v1/deleteBranchOffice/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_HomeController_deleteBranchOffice9_invoker = createInvoker(
    HomeController_0.deleteBranchOffice(fakeValue[Long]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.HomeController",
      "deleteBranchOffice",
      Seq(classOf[Long]),
      "POST",
      this.prefix + """prueba/v1/deleteBranchOffice/""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:8
    case controllers_Assets_versioned0_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned0_invoker.call(Assets_1.versioned(path, file))
      }
  
    // @LINE:17
    case controllers_Assets_versioned1_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned1_invoker.call(Assets_1.versioned(path, file))
      }
  
    // @LINE:19
    case controllers_HomeController_createCompany2_route(params@_) =>
      call { 
        controllers_HomeController_createCompany2_invoker.call(HomeController_0.createCompany)
      }
  
    // @LINE:20
    case controllers_HomeController_updateCompany3_route(params@_) =>
      call { 
        controllers_HomeController_updateCompany3_invoker.call(HomeController_0.updateCompany)
      }
  
    // @LINE:21
    case controllers_HomeController_getCompany4_route(params@_) =>
      call(params.fromPath[String]("rif", None)) { (rif) =>
        controllers_HomeController_getCompany4_invoker.call(HomeController_0.getCompany(rif))
      }
  
    // @LINE:22
    case controllers_HomeController_createBranchOffice5_route(params@_) =>
      call { 
        controllers_HomeController_createBranchOffice5_invoker.call(HomeController_0.createBranchOffice)
      }
  
    // @LINE:23
    case controllers_HomeController_updateBranchOffice6_route(params@_) =>
      call { 
        controllers_HomeController_updateBranchOffice6_invoker.call(HomeController_0.updateBranchOffice)
      }
  
    // @LINE:24
    case controllers_HomeController_getBranchOffice7_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_HomeController_getBranchOffice7_invoker.call(HomeController_0.getBranchOffice(id))
      }
  
    // @LINE:25
    case controllers_HomeController_deleteCompany8_route(params@_) =>
      call(params.fromPath[String]("rif", None)) { (rif) =>
        controllers_HomeController_deleteCompany8_invoker.call(HomeController_0.deleteCompany(rif))
      }
  
    // @LINE:26
    case controllers_HomeController_deleteBranchOffice9_route(params@_) =>
      call(params.fromPath[Long]("id", None)) { (id) =>
        controllers_HomeController_deleteBranchOffice9_invoker.call(HomeController_0.deleteBranchOffice(id))
      }
  }
}
