// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Maria Victoria/Documents/play-java-seed/conf/routes
// @DATE:Wed Sep 05 19:18:27 VET 2018

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:8
package controllers.javascript {

  // @LINE:19
  class ReverseHomeController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def createBranchOffice: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.createBranchOffice",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/createBranchOffice"})
        }
      """
    )
  
    // @LINE:25
    def deleteCompany: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.deleteCompany",
      """
        function(rif0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/deleteCompany/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("rif", rif0))})
        }
      """
    )
  
    // @LINE:23
    def updateBranchOffice: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.updateBranchOffice",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/updateBranchOffice"})
        }
      """
    )
  
    // @LINE:20
    def updateCompany: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.updateCompany",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/updateCompany"})
        }
      """
    )
  
    // @LINE:19
    def createCompany: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.createCompany",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/createCompany"})
        }
      """
    )
  
    // @LINE:26
    def deleteBranchOffice: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.deleteBranchOffice",
      """
        function(id0) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/deleteBranchOffice/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:21
    def getCompany: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.getCompany",
      """
        function(rif0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/getCompany/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[String]].javascriptUnbind + """)("rif", rif0))})
        }
      """
    )
  
    // @LINE:24
    def getBranchOffice: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.HomeController.getBranchOffice",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "prueba/v1/getBranchOffice/" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Long]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
  }

  // @LINE:8
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
        
          if (true) {
            return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
          }
        
        }
      """
    )
  
  }


}
