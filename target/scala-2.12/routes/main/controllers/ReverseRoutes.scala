// @GENERATOR:play-routes-compiler
// @SOURCE:C:/Users/Maria Victoria/Documents/play-java-seed/conf/routes
// @DATE:Wed Sep 05 19:18:27 VET 2018

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:8
package controllers {

  // @LINE:19
  class ReverseHomeController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:22
    def createBranchOffice(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "prueba/v1/createBranchOffice")
    }
  
    // @LINE:25
    def deleteCompany(rif:String): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "prueba/v1/deleteCompany/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("rif", rif)))
    }
  
    // @LINE:23
    def updateBranchOffice(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "prueba/v1/updateBranchOffice")
    }
  
    // @LINE:20
    def updateCompany(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "prueba/v1/updateCompany")
    }
  
    // @LINE:19
    def createCompany(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "prueba/v1/createCompany")
    }
  
    // @LINE:26
    def deleteBranchOffice(id:Long): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "prueba/v1/deleteBranchOffice/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
    // @LINE:21
    def getCompany(rif:String): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "prueba/v1/getCompany/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[String]].unbind("rif", rif)))
    }
  
    // @LINE:24
    def getBranchOffice(id:Long): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + "prueba/v1/getBranchOffice/" + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Long]].unbind("id", id)))
    }
  
  }

  // @LINE:8
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def versioned(file:Asset): Call = {
    
      (file: @unchecked) match {
      
        // @LINE:8
        case (file)  =>
          implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
          Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
      
      }
    
    }
  
  }


}
